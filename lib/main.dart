import 'package:flutter/material.dart';
import 'package:images_gallery/my_app/my_app.dart';
import 'package:images_gallery/utils/locator/locator.dart';

void main() {
  initLocator();
  runApp(const MyApp());
}
